import java.util.Scanner;

public class SizeString {
    public static void main(String[] args) {
        int count  = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un texto");
        String text = sc.nextLine();
        char[] toCharacter = text.toCharArray();
        for (char i: toCharacter) {
            if (i == 'a' || i == 'e' || i == 'i' || i == 'o' || i == 'u'){
                count++;
            }
        }
        System.out.println("El tamaño del texto es de " + text.length() + ", el numero de vocales es de "+ count);
    }
}
