import java.util.Scanner;

public class DayWeek {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el dia de la semana que quiere validar");
        String day = sc.nextLine();
        switch (day){
            case "lunes": System.out.println("Dia laboral"); break;
            case "martes": System.out.println("Dia laboral"); break;
            case "miercoles": System.out.println("Dia laboral"); break;
            case "jueves": System.out.println("Dia laboral"); break;
            case "viernes": System.out.println("Dia laboral"); break;
            case "sabado": System.out.println("Dia laboral"); break;
            case "domingo": System.out.println("Dia festivo"); break;
        }
    }
}
