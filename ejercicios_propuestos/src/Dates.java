import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


class Dates {
    public static void main(String[] args) {
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        System.out.println(hourdateFormat.format(date));
    }
}