public class ParesEImpares {
    public static void main(String[] args) {
        int i = 0;
        System.out.println("Numeros Pares");
        while (i <= 100){
            if (i % 2 == 0){
                System.out.println(i);
            }
            i++;
        }
        System.out.println("Numeros Impares");
        i = 0;
        while (i <= 100){
            if (i % 2 != 0){
                System.out.println(i);
            }
            i++;
        }
    }
}
