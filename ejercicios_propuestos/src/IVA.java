import java.util.Scanner;

public class IVA {

    public static void main(String[] args) {
        final double IVA = 0.21;
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserte el valor del producto");
        Double price = Double.parseDouble(sc.nextLine());
        double totalValue = (price * IVA) + price;
        System.out.println("El valor total del producto es " + totalValue);
    }
}
