import java.util.Scanner;

public class LoopNum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el index para empezar a contar");
        int index = sc.nextInt();
        while (index <= 1000){
            System.out.println(index);
            index += 2;
        }
    }
}
