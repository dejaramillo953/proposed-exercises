import java.util.Scanner;

public class RadiusCalculate {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.printf("inserte el radio del circulo \n");
        Double radius = Double.parseDouble(sc.nextLine());
        double area = Math.PI * Math.pow(radius,2);
        System.out.printf("El area del circulo es " + area);
    }
}
