package model;

public class Person {

    final static Character GENDER = 'H';
    public final static int UNDERWEIGHT = -1;
    public final static int IDEAL_WEIGHT = 0;
    public final static int OVERWEIGHT = 1;

    private String name;
    private Integer age;
    private String dni;
    private Character gen;
    private Double weight;
    private Double height;

    public Person() {
        this("", 0, GENDER, 0.0, 0.0);
    }

    public Person(String name, Integer age, Character gen) {
        this.name = name;
        this.age = age;
        this.gen = gen;
    }

    public Person(String name, Integer age, Character gen, Double weight, Double height) {
        this.name = name;
        this.age = age;
        generateDNI();
        this.gen = gen;
        this.weight = weight;
        this.height = height;
    }

    //metodos privados

    private void generateDNI() {
        final int divisor = 23;

        int numDNI = ((int) Math.floor(Math.random() * (100000000 - 100000000)));
        int res = numDNI - (numDNI / divisor * divisor);

        char letterDNI = generateLetter(res);

        this.dni = Integer.toString(numDNI) + letterDNI;
    }

    private char generateLetter(int res) {
        char letters[] = {'T', 'R', 'W', 'A', 'G', 'M', 'Y',
                'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z',
                'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};
        return letters[res];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDNI() {
        return dni;
    }


    public Character getGen() {
        return gen;
    }

    public void setGen(Character gen) {
        this.gen = gen;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    //metodos publicos

    public int IMCCalculate() {
        double imc = weight / Math.pow(height, 2);
        if (imc < 20) {
            return UNDERWEIGHT;
        } else if (imc >= 20 && imc <= 25) {
            return IDEAL_WEIGHT;
        } else if (imc > 25) {
            return OVERWEIGHT;
        }
        return 0;
    }

    public boolean legalAge() {
        if (this.age > 18)
            return true;
        else
            return false;
    }

    public void validateGender() {
        if (gen != 'H' || gen != 'M')
            gen = GENDER;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", DNI='" + dni + '\'' +
                ", gen=" + gen +
                ", weight=" + weight +
                ", height=" + height +
                '}';
    }

}
