package menu;
import model.Person;

import java.util.Locale;
import java.util.Scanner;


public class Menu {
    public static void showMenu() {

        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        sc.useLocale(Locale.US);

        System.out.println("Ingrese el nombre");
        String name = sc.next();

        System.out.println("Ingrese la edad");
        int age = sc.nextInt();

        System.out.println("Ingrese el genero [H-M]");
        char gender = sc.next().charAt(0);

        System.out.println("Ingrese la Altura [0.00]Mts.");
        double heigt = sc.nextDouble();

        System.out.println("Ingrese el peso [0.00]KG.");
        double weight = sc.nextDouble();

        //instanciar clases
        Person person = new Person();
        Person person1 = new Person(name,age,gender);
        Person person2 = new Person(name,age,gender,heigt,weight);

        //datos del objeto 1
        person.setName("Eduardo Ramirez");
        person.setAge(24);
        person.setGen('H');
        person.setHeight(1.84);
        person.setWeight(94.0);
        System.out.println(person);
        System.out.println("***********************************");

        //Datos del objeto 2
        person1.setHeight(1.80);
        person1.setWeight(80.0);
        System.out.println(person1);
        System.out.println("*************************************");
        System.out.println(person2);

        //Mostrar lso datos del objeto
        System.out.println(person);
        showIMC(person);
        showLegalage(person);

        //objeto 2
        System.out.println(person1);
        showIMC(person1);
        showLegalage(person1);

        //objeto 2
        System.out.println(person2);
        showIMC(person2);
        showLegalage(person2);
    }

    public static void showIMC(Person person){
        int IMC = person.IMCCalculate();
        switch (IMC){
            case -1:
                System.out.println("Esta por debajo de su peso ideal");
                break;
            case 0:
                System.out.println("Está en su peso ideal");
                break;
            case 1:
                System.out.println("Esta por encima de su peso ideal");
                break;
        }
    }

    public static void showLegalage(Person person){
        if (person.legalAge())
            System.out.println("Es mayor de edad");
        else
            System.out.println("Es menor de edad");
    }

}

